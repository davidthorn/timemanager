<?php

require_once "TimeEntry.php";

class TimeLog{
    
    
    
        public $logs = array();


        public $fp = null;

        public $log_path = null;

        public function __construct( $log_folder_path )
        {

            if( !is_dir( $log_folder_path ) ){

                throw  new Exception("$log_folder_path is not a dir");
            }

             if( !is_writable($log_folder_path) && is_readable($log_folder_path) ){
                 throw  new Exception("$log_folder_path is not a writable");
              }

               if( !is_readable($log_folder_path) ){
                 throw  new Exception("$log_folder_path is not a readable");
              }


              $this->log_path = $log_folder_path;

              $year = date("Y");


              $root_folder = $this->log_path;

              $folder = $this->writeFolder( $root_folder , $year );
              $folder = $this->writeFolder($folder, date("m"));
              $folder = $this->writeFolder($folder,date("d"));
              $folder = $this->writeFile($folder, "log" . "-". date("Y") ."-". date("m") . "-". date("d")   );

        }

        public function writeFolder( $folder , $name ){
             if( !is_dir( $folder .  $name) ){
                  mkdir( $folder . $name );
             }

             return $folder .=  $name . "/";
        }


          public function writeFile( $folder , $file ){
             if( !file_exists( $folder .  $file) ){

                 $contents = json_encode($this);

                $fp = fopen( $folder.$file , "w" );
                fwrite( $fp , $contents );
                fclose($fp);
             }

             return $folder .=  $file;
        }

        public function add( TimeEntry $entry ){
              $this->logs[] = $entry;
        }

        public function getAll(){
            return $this->logs;
        }

        public function serialize(){
            return json_encode($this);
        }

        public function deserialize( $json ){
            $obj = json_decode($json);

            if( is_object( $obj ) ){
                foreach( $obj as $k => $v ){
                    if(property_exists($this, $k) ){
                         $this->$k = $v;
                    }
                   
                }
            }
        }


        public function persist( $year , $month , $day , TimeEntry $entry ){

              $root_folder = $this->log_path;

              
              $folder = $this->writeFolder( $root_folder , $year );
              $folder = $this->writeFolder($folder, $month);
              $folder = $this->writeFolder($folder, $day);
             // $folder = $this->writeFile($folder, "log" . $year .$month . $day  );
              /*
              if( !is_dir( $root_folder .  $year) ){

                  echo "Making dir:  $root_folder$year\n";
                  //mkdir( $root_folder . $year );

                  $root_folder .= $year . "/";
             }


             // $month = date("m");

              if( !is_dir( $root_folder .  $month) ){
                  echo "Making dir:  $root_folder .  $month\n";
                  //mkdir( $root_folder . $month );

                  $root_folder .=  $month . "/";
             }


              //$day = date("d");

              if( !is_dir( $root_folder .  $day) ){

                    echo "Making dir:  $root_folder .  $day\n";
                 // mkdir( $root_folder . $day );

                  $root_folder .= $day . "/";
             }



             $filename = "log_$year-$month-$day";

                 echo "Making file:  $root_folder .  $filename\n";


             if( !file_exists( $root_folder . $filename ) ){
                 $fp = fopen( $root_folder . $filename , "w" );
                 fwrite( $fp , $this->serialize() );
                 fclose( $fp );
             }


             $contents = file_get_contents($root_folder . $filename);

             $obj = new TimeLog( $this->log_path );
             $obj->deserialize($contents);

             $obj->add($entry);


              $fp = fopen( $root_folder . $filename , "w" );
              fwrite( $fp , $obj->serialize() );
              fclose( $fp );

              $this->logs = $obj->logs;*/

        }


       
    
}

